package com.historico.service;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.google.gson.Gson;
import com.historico.pojo.Datos;
import com.historico.pojo.Historico;
import com.historico.pojo.TipoCambio;

/**
 * @author Alejandro Enríquez
 *
 */
@Service
public class RestClient {

  /**
   * @throws IOException
   */
  public static void getHistorico() throws IOException {
    FileWriter fw =
        new FileWriter("C:\\Users\\Alejandro Enríquez\\Documents\\historicoTCambio.csv");
    Gson gson = new Gson();

    final String uri =
        "https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/2000-01-01/2020-03-02";
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    headers.set("Bmx-Token", "b446fde77758814a2950b35ec8abd15ca6e4029725af5cb093402149b492158d");
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

    ResponseEntity<String> result =
        restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    // System.out.println(result.getBody());

    Historico hst = gson.fromJson(result.getBody(), Historico.class);

    List<Datos> datos = hst.getBmx().getSeries().get(0).getDatos();

    for (Datos dto : datos) {
      fw.write(dto.toString() + "\r");
    }

    // fw.flush();
    fw.close();
    System.out.println("Histórico creado!");
  }

  /**
   * @return TipoCambio
   */
  public static TipoCambio getCotizacion() {
    Gson gson = new Gson();

    SimpleDateFormat parseador = new SimpleDateFormat("yyyy-MM-dd");
    String hoy = parseador.format(new Date());

    final String uri =
        "https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/" + hoy + "/" + hoy;
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    headers.set("Bmx-Token", "b446fde77758814a2950b35ec8abd15ca6e4029725af5cb093402149b492158d");
    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

    ResponseEntity<String> result =
        restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
    // System.out.println(result.getBody());

    Historico hst = gson.fromJson(result.getBody(), Historico.class);

    List<Datos> datos = hst.getBmx().getSeries().get(0).getDatos();

    TipoCambio tipoCambio = new TipoCambio();
    if (!datos.isEmpty()) {
      tipoCambio.setFecha_cambio(datos.get(0).formatter(datos.get(0).getFecha()));
      tipoCambio.setTipo_cambio(datos.get(0).getDato());
      tipoCambio.setMoneda("USD");
    }
    return tipoCambio;

  }
}
