package com.historico.service;

import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import com.historico.main.TipoCambioMapper;
import com.historico.pojo.TipoCambio;

public class CotizaServiceJob implements Runnable {
  private JdbcTemplate jdbcTemplate;

  public CotizaServiceJob(JdbcTemplate jdbcTemplate) {
    super();
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public void run() {
    insertCotiza();

  }

  void insertCotiza() {
    TipoCambio cotizacion = RestClient.getCotizacion();
    System.out.println(">>>>>>>>>>>     Cotización del día de hoy: " + cotizacion);

    if (cotizacion.getFecha_cambio() != null) {
      String sql =
          "select tipo_cambio, fecha_cambio, moneda from cat.tipo_cambio where fecha_cambio = '"
              + cotizacion.getFecha_cambio() + "'";

      TipoCambioMapper mapperTCambio = new TipoCambioMapper();
      List<TipoCambio> tipoCambio = jdbcTemplate.query(sql, mapperTCambio);

      if (tipoCambio.isEmpty()) {
        sql = "insert into cat.tipo_cambio(tipo_cambio, fecha_cambio, moneda) values ("
            + cotizacion.getTipo_cambio() + ",'" + cotizacion.getFecha_cambio() + "','"
            + cotizacion.getMoneda() + "')";
        jdbcTemplate.execute(sql);
      }
    }

  }

}
