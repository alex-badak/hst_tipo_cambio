package com.historico.pojo;

import java.util.ArrayList;
import java.util.List;

public class Series {
  private String idSerie;
  private String titulo;
  private List<Datos> datos = new ArrayList<>();

  public String getIdSerie() {
    return idSerie;
  }

  public void setIdSerie(String idSerie) {
    this.idSerie = idSerie;
  }

  public String getTitulo() {
    return titulo;
  }

  public void setTitulo(String titulo) {
    this.titulo = titulo;
  }

  public List<Datos> getDatos() {
    return datos;
  }

  public void setDatos(List<Datos> datos) {
    this.datos = datos;
  }

}
