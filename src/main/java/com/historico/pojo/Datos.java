package com.historico.pojo;

public class Datos {
  private String fecha;
  private String dato = "";

  public String getFecha() {
    return fecha;
  }

  public void setFecha(String fecha) {
    this.fecha = fecha;
  }

  public String getDato() {
    return dato;
  }

  public void setDato(String dato) {
    this.dato = dato;
  }

  public String toString() {
    return formatter(this.fecha) + "," + this.dato + ",USD";

  }

  public String formatter(String fecha) {
    String anio = fecha.substring(6);
    String mes = fecha.substring(3, 5);
    String dia = fecha.substring(0, 2);
    return anio + "-" + mes + "-" + dia;

  }

}
