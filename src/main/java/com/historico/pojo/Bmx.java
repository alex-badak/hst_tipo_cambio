package com.historico.pojo;

import java.util.ArrayList;
import java.util.List;

public class Bmx {
  private List<Series> series = new ArrayList<>();

  public List<Series> getSeries() {
    return series;
  }

  public void setSeries(List<Series> series) {
    this.series = series;
  }

}
