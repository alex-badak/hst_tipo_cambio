package com.historico.pojo;

public class TipoCambio {
  String tipo_cambio;
  String fecha_cambio;
  String moneda;

  public String getTipo_cambio() {
    return tipo_cambio;
  }

  public void setTipo_cambio(String tipo_cambio) {
    this.tipo_cambio = tipo_cambio;
  }

  public String getFecha_cambio() {
    return fecha_cambio;
  }

  public void setFecha_cambio(String fecha_cambio) {
    this.fecha_cambio = fecha_cambio;
  }

  public String getMoneda() {
    return moneda;
  }

  public void setMoneda(String moneda) {
    this.moneda = moneda;
  }

  public String toString() {
    return this.fecha_cambio + "," + this.tipo_cambio;
   
  }
}
