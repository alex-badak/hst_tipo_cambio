package com.historico.main;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.historico.pojo.TipoCambio;

public class TipoCambioMapper implements RowMapper<TipoCambio> {

  @Override
  public TipoCambio mapRow(ResultSet rs, int rowNum) throws SQLException {
    TipoCambio tc = new TipoCambio();
    tc.setFecha_cambio(rs.getString("fecha_cambio"));
    tc.setTipo_cambio(rs.getString("tipo_cambio"));
    tc.setMoneda(rs.getString("moneda"));
    return tc;
  }

}
