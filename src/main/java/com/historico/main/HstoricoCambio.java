package com.historico.main;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.concurrent.DefaultManagedTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.historico.service.CotizaServiceJob;

@ServletComponentScan
@EnableTransactionManagement
@SpringBootApplication
public class HstoricoCambio implements CommandLineRunner {

  @Autowired
  JdbcTemplate jdbcTemplate;

  public static void main(String[] args) throws IOException {
    SpringApplication.run(HstoricoCambio.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    try {
      CotizaServiceJob job = new CotizaServiceJob(jdbcTemplate);
      System.out.println("Creacion de una instacia de Scheduler");
      // Creacion de una instacia de Scheduler
      TaskScheduler scheduler = new DefaultManagedTaskScheduler();

      System.out.println("Start scheduler");
      Trigger trigger = new CronTrigger("0 19 12 ? * MON-FRI");

      // Registro dentro del Scheduler
      scheduler.schedule(job, trigger);

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}
